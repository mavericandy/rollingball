import * as React from 'react';
import { Dimensions, StyleSheet, Text, View } from 'react-native';
import { Accelerometer } from 'expo-sensors';

const initialAccelerometerState = {
  x: 0,
  y: 0,
  z: -1,
};

const { width, height } = Dimensions.get('window');

const TABLE_WIDTH = width - 60;
const TABLE_HEIGHT = height - 100;

const BALL_SIZE = 30;

const TABLE_BORDER_X = (TABLE_WIDTH - BALL_SIZE) / 2;
const TABLE_BORDER_Y = (TABLE_HEIGHT - BALL_SIZE) / 2;

export function useAccelerometer() {
  const [data, setData] = React.useState(initialAccelerometerState);
  const [subscription, setSubscription] = React.useState(null);

  const subscribe = () => {
    Accelerometer.setUpdateInterval(16);
    Accelerometer.addListener((data) => setData(data));
  };

  const unsubscribe = () => {
    if (subscription) subscription.remove();
    setSubscription(null);
  };

  React.useEffect(() => {
    subscribe();
    return () => unsubscribe();
  }, []);

  return data;
}

export default function App() {
  const data = useAccelerometer();

  const [position, setPosition] = React.useState(initialAccelerometerState);

  React.useEffect(() => {
    if (data) {
      setPosition((prev) => {
        const newPosition = {
          x: prev.x + data.x * 10,
          y: prev.y - data.y * 10,
          z: prev.z,
        };

        const newX =
          Math.abs(newPosition.x) > TABLE_BORDER_X ? prev.x : newPosition.x;
        const newY =
          Math.abs(newPosition.y) > TABLE_BORDER_Y ? prev.y : newPosition.y;

        return {
          ...newPosition,
          x: newX,
          y: newY,
        };
      });
    }
  }, [data]);

  return (
    <View style={styles.container}>
      <View style={styles.platform} />
      <View
        style={[
          styles.position,
          {
            transform: [{ translateX: position.x }, { translateY: position.y }],
          },
        ]}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  platform: {
    width: TABLE_WIDTH,
    height: TABLE_HEIGHT,
    backgroundColor: '#efefef',
  },
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#063970',
    alignItems: 'center',
    justifyContent: 'center',
  },
  position: {
    position: 'absolute',
    backgroundColor: '#76b5c5',
    height: BALL_SIZE,
    width: BALL_SIZE,
    borderRadius: BALL_SIZE / 2,
  },
});
